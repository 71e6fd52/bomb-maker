extends Node2D

export (PackedScene) var Wall
export (PackedScene) var Player
export (PackedScene) var Bomb
export (PackedScene) var Ball
export (PackedScene) var Heart
export (PackedScene) var Lightning
export (PackedScene) var BM

var player
var debug
var started

func _ready():
	randomize()
	g.screensize = get_viewport_rect().size
	player = Player.instance()
	add_child(player)
	initialize_map()
	start()

func _process(delta):
	if Input.is_action_just_released("reset"):
		start()
	if Input.is_action_just_released("quit"):
		get_tree().quit()
	if Input.is_action_just_pressed("toggle_fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

	if started:
		if Input.is_action_just_pressed("debug"):
			debug = true
			$HUD.debug = true
		if debug:
			debug_process(delta)

		if $HUD.heart <= 0:
			$HUD.stop()
			stop()
		if g.Heart.more($HUD, delta):
			g.Heart.make(self)
		if g.Lightning.more($HUD, delta):
			g.Lightning.make(self)
		if g.BM.more($HUD, delta):
			g.BM.make(self)

func debug_process(_delta):
	if Input.is_action_just_pressed("add_heart"):
		$HUD.heart += 10
	if Input.is_action_just_pressed("clean_all"):
		get_tree().call_group("item", "queue_free")
		for _i in range(0, 3):
			g.Bomb.make(self)
		for _i in range(0, 4):
			g.Ball.make(self)
	if Input.is_action_just_pressed("clean_ball"):
		get_tree().call_group("ball", "queue_free")
		for _i in range(0, 4):
			g.Ball.make(self)
	if Input.is_action_just_pressed("clean_bomb"):
		get_tree().call_group("bomb", "queue_free")
		for _i in range(0, 3):
			g.Bomb.make(self)

func initialize_map():
	for i in range(0, g.W):
		for j in range(0, g.H):
			if g.map[j][i] == 1:
				var wall = Wall.instance()
				add_child(wall)
				wall.position.x = i * g.SIZE + g.HSIZE
				wall.position.y = j * g.SIZE + g.HSIZE

func start():
	started = true
	$HUD.start()
	debug = false
	get_tree().call_group("item", "queue_free")
	player.position = g.random_location()
	for _i in range(0, 3):
		g.Bomb.make(self)
	for _i in range(0, rand_range(4,7)):
		g.Ball.make(self)

func stop():
	started = false
	get_tree().call_group("item", "queue_free")

func make(item, velocity = 400):
	if item is PackedScene: item = item.instance()
	add_child(item)
	item.position = g.random_location()
	if typeof(velocity) == TYPE_INT:
		velocity = g.random_velocity(velocity)
	item.velocity = velocity
	item.hud = $HUD
