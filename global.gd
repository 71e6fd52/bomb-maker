extends Node

const map = [
   [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ],
   [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ],
]

var Bomb = load("Bomb.gd")
var Ball = load("Ball.gd")
var Heart = load("props/Heart.gd")
var Lightning = load("props/Lightning.gd")
var BM = load("props/BM.gd")

const SIZE = 32
const HSIZE = SIZE / 2
const W = 32
const H = 18

var screensize

func _ready():
	pass

static func random_location():
	var position = Vector2()
	position.x = rand_range(HSIZE + SIZE, (W - 2) * SIZE + HSIZE)
	position.y = rand_range(HSIZE + SIZE, (H - 2) * SIZE + HSIZE)
	if map[position.y / SIZE][position.x / SIZE] == 1:
		return random_location()
	return position

static func random_velocity(mx = 400):
	return Vector2(rand_range(-mx, mx), rand_range(-mx, mx))

static func random(rate, delta):
	if delta == 0: return 0
	return randf() * rate / delta < 1

static func collide(object, delta):
	if not ("velocity" in object && "position" in object):
		return
	var velocity = object.velocity
	var position = object.position

	var collision = object.move_and_collide(velocity * delta)
	var times = 0
	while collision && times < 10:
		if object.has_method("on_collide"):
			object.call("on_collide", collision)
		var reflect = collision.remainder.bounce(collision.normal)
		var collider = collision.get_collider()
		if "mass" in object && "velocity" in collider && "position" in collider && "mass" in collider:
			# from https://en.wikipedia.org/wiki/Elastic_collision
			var v1 = velocity
			var v2 = collider.velocity
			var m1 = object.mass
			var m2 = collider.mass
			var dx = position - collider.position
			var dxls = dx.length_squared()
			velocity = v1 - 2 * m2 / (m1 + m2) * (v1 - v2).dot(dx) / dxls * dx
			collider.velocity = v2 - 2 * m1 / (m1 + m2) * (v2 - v1).dot(-dx) / dxls * (-dx)
		else:
			velocity = velocity.bounce(collision.normal)
		collision = object.move_and_collide(reflect)
		times += 1

	if position.x < 0:
		velocity.x = abs(velocity.x)
	if position.x > g.screensize.x:
		velocity.x = -abs(velocity.x)
	if position.y < 0:
		velocity.y = abs(velocity.y)
	if position.y > g.screensize.y:
		velocity.y = -abs(velocity.y)

	object.velocity = velocity
