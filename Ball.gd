extends "res://Item.gd"

const mass = 80

static func make(main):
	main.make(main.Ball, 800)

func _ready():
	add_to_group("ball")

func _process(delta):
	if g.random(40, delta):
		make(get_parent())

func touch():
	make(get_parent())
	if (rand_range(0, 20) + 1) >= 20:
		make(get_parent())
	hud.score += 10

func item_type(): return "Ball"
