extends CanvasLayer

var score setget set_score
var heart setget set_heart

var high = 0
var debug setget set_debug

const HEART = 20

func _ready():
	load_high()
	start()

func start():
	self.score = 0
	self.heart = HEART
	debug = false
	$Control/Debug.hide()
	$Control/Die.hide()
	$Control/High.hide()

func stop():
	if !debug && score > high:
		high = score
		$Control/High.text = tr("NEW_HIGH") + str(high)
		save_high()
	else:
		update_high()
	$Control/Die.show()
	$Control/High.show()

func save_high():
	var file = File.new()
	file.open("user://high.dat", file.WRITE)
	file.store_32(high)
	file.close()

func load_high():
	var file = File.new()
	file.open("user://high.dat", file.READ)
	high = file.get_32()
	file.close()

func set_debug(new):
	if new:
		debug = true
		$Control/Debug.show()

func set_score(new):
	score = new
	$Control/Score.text = str(score)

func set_heart(new):
	heart = new
	$Control/Heart.text = str(heart)

func update_high():
	$Control/High.text = tr("HIGH") + str(high)
