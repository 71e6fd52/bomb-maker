extends "res://Item.gd"

const mass = 100

static func make(main):
	main.make(main.Bomb, 500)

func _ready():
	add_to_group("bomb")

func _process(delta):
	if g.random(40, delta):
		make(get_parent())

func touch():
	if rand_range(0, 25) > 0:
		make(get_parent())
	hud.heart -= 1

func item_type(): return "Bomb"
