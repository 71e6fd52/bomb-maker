extends KinematicBody2D

const mass = 200

var velocity

func _init():
	set_meta("type", "Player")

func _ready():
	velocity = Vector2()

func _process(_delta):
	if velocity.length() <= 400:
		if Input.is_action_pressed("ui_right"):
			velocity.x = 400
		if Input.is_action_pressed("ui_left"):
			velocity.x = -400
		if Input.is_action_pressed("ui_down"):
			velocity.y = 400
		if Input.is_action_pressed("ui_up"):
			velocity.y = -400

func _physics_process(delta):
	g.collide(self, delta)
	velocity.x *= pow(0.84, 42 * delta)
	velocity.y *= pow(0.84, 42 * delta)

func on_collide(collision):
	if collision.get_collider().is_class("Item"):
		collision.get_collider().item_touch()

func is_class(type): return type == get_class() or .is_class(type)
func get_class(): return "Player"
