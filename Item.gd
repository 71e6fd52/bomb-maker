extends KinematicBody2D

signal touch

var velocity

var touched = false
var hud

func _init():
	set_meta("is_item", true)

func _ready():
	add_to_group("item")

func _process(delta):
	if "DIE" in self && g.random(get("DIE"), delta):
		queue_free()

func _physics_process(delta):
	g.collide(self, delta)

func on_collide(collision):
	if collision.get_collider().is_class("Player"):
		item_touch()

func item_touch(callback = "touch"):
	if touched: return
	touched = true
	emit_signal("touch")
	if !call(callback):
		queue_free()

func is_class(type): return type == item_type() or type == get_class() or .is_class(type)
func get_class(): return "Item"
func item_type(): return "Item"
