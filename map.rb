#!/usr/bin/env ruby

puts 'var map = ['
18.times do
  print "   ["
  loop do
    c = STDIN.getc
    break if c == "\n"
    print "#{c}, "
  end
  puts "],"
end
puts ']'
