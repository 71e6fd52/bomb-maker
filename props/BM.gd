extends "res://Item.gd"

const mass = 100
const BOMB = 5

static func make(main):
	main.make(main.BM, 200)

static func more(hud, delta):
	return g.random(180000 * pow(0.998, hud.score) + 10, delta)

func _ready():
	add_to_group("prop")
	add_to_group("bm")

func _process(delta):
	if g.random(BOMB, delta):
		g.Bomb.make(get_parent())

func touch():
	hud.heart -= 2

func item_type(): return "BM"
