extends "res://Item.gd"

const mass = 100
const DIE = 15

static func make(main):
	main.make(main.Heart, 280)

static func more(_hud, delta):
	return g.random(50, delta)

func _ready():
	add_to_group("prop")
	add_to_group("heart")

func touch():
	hud.heart += 3

func item_type(): return "Heart"
