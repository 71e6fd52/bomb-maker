extends "res://Item.gd"

const mass = 1
const DIE = 10

static func make(main):
	main.make(main.Lightning, 1500)

static func more(_hud, delta):
	return g.random(80, delta)

func _ready():
	add_to_group("prop")
	add_to_group("lightning")

func touch():
	hud.heart -= 3
	hud.score += 800

func item_type(): return "Lightning"
